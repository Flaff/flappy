package com.flaff.flappygame;


import com.flaff.flappygame.io.Challenge;
import com.flaff.flappygame.io.ScoreIOObject;
import com.flaff.flappygame.io.ServerIOThread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Stack;

public class Server {

	int port = 9001;
	boolean	isRunning = false;

	ServerSocket serverSocket = null;
	Socket clientSocket = null;
	Thread runningThread = null;

	static public final Object locker = new Object();
	static public int clients = 0;

    private static boolean challengeStackLocker = false;

	public static Stack<Challenge> challenges = new Stack<>();

	public static void main(String v[])
	{
		new Server().startServer();
		
	}

	public void startServer()
	{
		isRunning = true;

		new Thread() {
			public void run() {
				synchronized (this) {
					runningThread = Thread.currentThread();
				}

				openSocket(port);

				while (isRunning) {
                    System.out.println("awaiting client");
					clientSocket = null;

					try {
						clientSocket = serverSocket.accept();
						synchronized (locker) {
							clients++;
						}
						new Thread(new ServerIOThread(clientSocket)).start();
					} catch (IOException e) {
						System.out.println("error: can't accept client");
					}
				}
			}
		}.start();
	}

	void openSocket(int port)
	{
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("success: server started on port "+port);
		} catch (IOException e) {
			System.out.println("cant open on port "+port+" - server already running?");
		}
	}

    public static boolean isChallengeStackLocker() {
        return challengeStackLocker;
    }

    public static void setChallengeStackLocker(boolean challengeStackLocker) {
        Server.challengeStackLocker = challengeStackLocker;
    }
}

