package com.flaff.flappygame;

import com.flaff.flappygame.io.ScoreIOObject;
import com.flaff.flappygame.io.ServerIOThread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Stack;

public class serverMainThread implements Runnable {

	int 			port 			= 9001;
	boolean			isRunning		= false;

	ServerSocket 	serverSocket 	= null;
	Socket			clientSocket	= null;
	Thread 			runningThread	= null;

	static public final Object locker = new Object();
	static public final Object listlocker = new Object();
	static public int clients 		= 0;

	static public Stack<ScoreIOObject> challenges = new Stack<>();

	public void run()
	{
		synchronized(this){
			this.runningThread = Thread.currentThread(); }
		openSocket();

		while(isRunning)
		{
			clientSocket = null;

			try {
				clientSocket = this.serverSocket.accept();
				synchronized (serverMainThread.locker) {
					clients++;
				}
				new Thread(  new ServerIOThread(clientSocket)  ).start();

			} catch (IOException e) {
				System.out.println("error: can't accept client");
			}


		}
	}


	public serverMainThread(int port)
	{
		this.port = port;
		isRunning = true;
	}

	void openSocket()
	{
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("success: server started on port "+port);
		} catch (IOException e) {
			System.out.println("error: couldn't create socket on port "+port);
		}
	}
}
