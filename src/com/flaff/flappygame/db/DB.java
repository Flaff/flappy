package com.flaff.flappygame.db;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class DB implements Closeable {
	
	// JDBC
	public static final String DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:flappy.db";
	    
	private Connection conn;
	private Statement stat;
	// JDBC END
	
	
	
	public DB() throws ClassNotFoundException, SQLException
	{
		// JDBC
		Class.forName(DB.DRIVER);
		
		
		
		conn = DriverManager.getConnection(DB_URL);
		stat = conn.createStatement();
    
        try{
        	createDB();
        }
        catch(SQLException e)
        {
        	closeObject(stat);
        	closeObject(conn);
        	throw e;
        }
        // JDBC END
	}
	
	private void createDB() throws SQLException // JDBC
	{
        String createScores = "CREATE TABLE IF NOT EXISTS scores (id INTEGER PRIMARY KEY AUTOINCREMENT, nick varchar(255), score int)";

        	stat.execute(createScores);
       
	}
	
    public void closeDB() // JDBC
    {
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("[DB:ERROR] can't close");
            e.printStackTrace();
        }
    }
    
	@SuppressWarnings("unused")
	private void closeObject(Closeable obj)
	{
		if(obj == null)
			return;
		
		try {
			obj.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void closeObject(AutoCloseable obj)
	{
		if(obj == null)
			return;
		
		try {
			obj.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
    public boolean newScore(String nick, int score)
    {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "insert into scores values (NULL, ?, ?);");
            prepStmt.setString(1, nick);
            prepStmt.setInt(2, score);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("[DB:ERROR] can't add score");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    public List<PlayerScore> selectScore() {
        List<PlayerScore> scores = new LinkedList<PlayerScore>();
        try {
            ResultSet result = stat.executeQuery("SELECT * FROM scores");
            int id, score;
            String nick;
            
            while(result.next()) {
                id = result.getInt("id");
                nick = result.getString("nick");
                score = result.getInt("score");
                scores.add(new PlayerScore(id, nick, score));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return scores;
    }

	@Override
	public void close() throws IOException {
		try {
			this.conn.close();
		} catch (SQLException e) {
			throw new IOException("db connection error",e);
		}
		
	}
}