package com.flaff.flappygame.db;

public class PlayerScore {
	private int id;
	private String nick;
	private int score;
	
	public int getId()
	{
		return this.id; 
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getNick()
	{
		return this.nick;
	}
	
	public void setNick(String nick)
	{
		this.nick = nick;
	}
	
	public int getScore()
	{
		return this.score;
	}
	
	public void setScore(int highscore)
	{
		this.score = highscore;
	}
	
	public PlayerScore(int id, String nick, int score)
	{
		this.id = id;
		this.nick = nick;
		this.score = score;
	}
	
	@Override
	public String toString()
	{
		return this.nick + "," + this.score;
	}
}
