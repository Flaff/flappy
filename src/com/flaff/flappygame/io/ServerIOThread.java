package com.flaff.flappygame.io;

import com.flaff.flappygame.Server;
import com.flaff.flappygame.db.DB;
import com.flaff.flappygame.db.PlayerScore;

import java.io.*;
import java.lang.reflect.Type;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

public class ServerIOThread implements Runnable {

	Socket clientSocket = null;
	String cNum = null;

	public ServerIOThread(Socket in) {
		this.clientSocket = in;
	}

	void msg(String in) {
		System.out.println(cNum + in);
	}

	public void run() {
		InputStream is = null;
		ObjectInputStream ois = null;
		OutputStream os = null;
		ObjectOutputStream oos = null;

		// reczne wysylanie danych dataOutput dataInput stream
		try {
			cNum = "[client " + Server.clients + "] ";

			msg("thread started, awaiting input");

			is = clientSocket.getInputStream();
			ois = new ObjectInputStream(is);

			os = clientSocket.getOutputStream();
			os.flush();
			oos = new ObjectOutputStream(os);

			ScoreIOObject in = (ScoreIOObject) ois.readObject();

			if (in == null) {
				msg("object null");
				return;
			}

			msg("detected packet type: " + in.type);

			switch (in.type) {
				case VERSUS_MYNAMEIS:
					versus(oos, os, ois, is, in);
					break;
				case SINGLE_SCORE:
					single(oos, in);
					break;
				case UNDEFINED:
					msg("fatal error: undefined type");
					break;
			}



		} catch (IOException e) {
			msg("IO error " + e);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			msg("can't identify class");
		} catch (SQLException e) {
			msg("SQLException occured");
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			msg("exception " + e);
		} finally {
			closeObject(is);
			closeObject(os);
			closeObject(oos);
			closeObject(clientSocket);

			synchronized (Server.locker) {
				Server.clients--;
			}
		}

		msg("exit");

	}

	private void closeObject(Closeable obj) {
		if (obj == null)
			return;

		try {
			obj.close();
		} catch (IOException e) {
			System.out.print("failed to close");
			e.printStackTrace();
		}
	}

	private void single(ObjectOutputStream oos, ScoreIOObject in) throws SQLException, IOException, ClassNotFoundException {
		msg("i received: " + in.score + " " + in.nick);

		// do the stuff with received object
		DB scores = new DB();

		scores.newScore(in.nick, in.score);


		//read score
		List<PlayerScore> scoresList = scores.selectScore();

		int best = 0, myBest = 0;
		String bestNick = null;

		for (PlayerScore c : scoresList) {
			if (best < c.getScore()) {
				best = c.getScore();
				bestNick = c.getNick();
			}

			if (c.getNick().equals(in.nick) && myBest < c.getScore()) {
				myBest = c.getScore();
			}
		}

		// send back

		msg("sending: " + myBest + "of " + in.nick + ", " + best + " of " + bestNick);
		ScoreIOObject out = new ScoreIOObject(TypeIO.SINGLE_HIGHSCORES, in.nick, myBest, bestNick, best);
		msg("sent");

		oos.writeObject(out);
		closeObject(scores);
	}

	private void versus(ObjectOutputStream oos, OutputStream os, ObjectInputStream ois, InputStream is, ScoreIOObject in) throws Exception, SQLException, IOException, ClassNotFoundException, InterruptedException {


        boolean noResults = true;
        boolean noChallenges = true;



		while (Server.isChallengeStackLocker()) // wait
            Thread.sleep(20);
        Server.setChallengeStackLocker(true);

		msg("checking results for " + in.nick);
        // check results
		Iterator<Challenge> iterator = Server.challenges.iterator();
		while(iterator.hasNext())
		{
			Challenge c = iterator.next();
			if(c.to.equals(in.nick)) {
				if(c.type.equals(TypeIO.VERSUS_WIN) || c.type.equals(TypeIO.VERSUS_LOSE) || c.type.equals(TypeIO.VERSUS_REFUSE)) {
					System.out.println("result found");
                    challengeResult(oos, os, ois, is, c);
                    noResults = false;
                    iterator.remove();
                    break;
                }
			}
		}
        Server.setChallengeStackLocker(false);

        if(!noResults)
            return;

		msg("no results found");
		msg("checking challenges");

        while (Server.isChallengeStackLocker()) // wait
            Thread.sleep(20);

        // check challenges
        iterator = Server.challenges.iterator();
        while(iterator.hasNext())
        {
            Challenge c = iterator.next();
			msg("dbg: " + c.from + "," + c.to + "," + c.type + "," + c.score);
            if(c.to.equals(in.nick)) {
				msg("entry found found");
                if(c.type.equals(TypeIO.VERSUS_CHALLENGE)) {

                    challengeRespond(oos, os, ois, is, c);
                    noChallenges = false;
                    iterator.remove();
                    break;
                }
            }
        }
        Server.setChallengeStackLocker(false);

        if(!noChallenges)
            return;

		msg("no challenges found, player can play");

        challengeNew(oos, os, ois, is, in);
	}

	// player sees result of challenge
	private void challengeResult(ObjectOutputStream oos, OutputStream os, ObjectInputStream ois, InputStream is, Challenge c) throws Exception, IOException
	{
		if(!(c.type == TypeIO.VERSUS_WIN || c.type == TypeIO.VERSUS_LOSE || c.type == TypeIO.VERSUS_REFUSE))
			throw new Exception("invalid type - expected " + TypeIO.VERSUS_WIN + " or " + TypeIO.VERSUS_LOSE );

		oos.writeObject(
				new ScoreIOObject(c.type, c.from, c.score));
	}

	// player responds to challenge
    private void challengeRespond(ObjectOutputStream oos, OutputStream os, ObjectInputStream ois, InputStream is, Challenge c) throws Exception, IOException, ClassNotFoundException
    {
		if(c.type != TypeIO.VERSUS_CHALLENGE)
			throw new Exception("invalid type - expected " + TypeIO.VERSUS_CHALLENGE);

		msg("sending challenge: " + c.from + " to " + c.to + " with score " + c.score + " type: " + c.type);
		oos.writeObject(
				new ScoreIOObject(c.type, c.from, c.score));


		is = clientSocket.getInputStream();
//		ois = new ObjectInputStream(is);
//
//		os = clientSocket.getOutputStream();
//		os.flush();
//		oos = new ObjectOutputStream(os);

		// wait for player to finish playing
		msg("waiting for response");
		ScoreIOObject receive = (ScoreIOObject) ois.readObject();
		msg("response is " + receive.type);

		TypeIO challengeResult = TypeIO.UNDEFINED;

		if(receive == null)
			throw new IOException("empty score arrived");
        if(!(receive.type.equals(TypeIO.VERSUS_CHALLENGE_RESPOND) || receive.type.equals(TypeIO.VERSUS_REFUSE)) )
            throw new Exception("invalid type - expected " + TypeIO.VERSUS_CHALLENGE_RESPOND);

		if(receive.type.equals(TypeIO.VERSUS_REFUSE))
			challengeResult = TypeIO.VERSUS_REFUSE;
		else if(receive.score > c.score)
			challengeResult = TypeIO.VERSUS_WIN;
		else
			challengeResult = TypeIO.VERSUS_LOSE;


		msg("new challenge result, from:" + receive.nick + " to:" + receive.nick2 + " with score " + challengeResult);
        // adds challenge result to db
		Server.challenges.add(
				new Challenge(receive.nick, receive.nick2, receive.score, challengeResult)
		);
    }

	// new challenge
	private void challengeNew(ObjectOutputStream oos, OutputStream os, ObjectInputStream ois, InputStream is, ScoreIOObject in) throws Exception
	{
		if(in.type != TypeIO.VERSUS_MYNAMEIS)
			throw new Exception("invalid type - expected " + TypeIO.VERSUS_MYNAMEIS);

		is = clientSocket.getInputStream();

		os = clientSocket.getOutputStream();
		os.flush();

		// send to player info, that he can play
		oos.writeObject(
				new ScoreIOObject(TypeIO.VERSUS_CANPLAY));




		// wait for player to finish playing
		ScoreIOObject receive = (ScoreIOObject) ois.readObject();

		if(receive == null)
			throw new Exception("empty score arrived");
		if(receive.nick == receive.nick2)
			throw new Exception("illegal operation - player challenged himself");
        if(receive.type != TypeIO.VERSUS_CHALLENGE_SEND)
            throw new Exception("invalid type - expected " + TypeIO.VERSUS_CHALLENGE_SEND);


		msg("new challenge added, from:" + receive.nick + " to:" + receive.nick2 + " with score " + receive.score);
		// adds new challenge for his opponent
		Server.challenges.add(
				new Challenge(receive.nick, receive.nick2, receive.score, TypeIO.VERSUS_CHALLENGE)
		);

	}
}
