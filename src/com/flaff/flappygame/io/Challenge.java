package com.flaff.flappygame.io;

/**
 * Created by Flaff on 2015-06-03.
 */

public class Challenge {

    String from, to;
    int score;
    TypeIO type = TypeIO.UNDEFINED;

    public Challenge(String from, String to, int score, TypeIO type) {
        this.from = from;
        this.to = to;
        this.score = score;
        this.type = type;
    }
}

