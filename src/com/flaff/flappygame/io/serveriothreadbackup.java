package com.flaff.flappygame.io;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.List;

import com.flaff.flappygame.db.DB;
import com.flaff.flappygame.db.PlayerScore;
import com.flaff.flappygame.serverMainThread;

public class serveriothreadbackup implements Runnable{

    Socket clientSocket = null;
    String cNum = null;

    public serveriothreadbackup(Socket in)
    {
        this.clientSocket = in;
    }

    void msg(String in)
    {
        System.out.println(cNum + in);
    }
    public void run()
    {
        InputStream is = null;
        ObjectInputStream ois = null;
        OutputStream os = null;
        ObjectOutputStream oos = null;

        DB scores = null;

        // reczne wysylanie danych dataOutput dataInput stream
        try {
            cNum = "[client " + serverMainThread.clients + "] ";

            msg("thread started");
            msg("awaiting input");

            is = clientSocket.getInputStream();
            ois = new ObjectInputStream(is);

            os = clientSocket.getOutputStream();
            os.flush();
            oos = new ObjectOutputStream(os);

            ScoreIOObject in = (ScoreIOObject) ois.readObject();
            if(in!=null)
            {
                msg("i has: " + in.score + " " + in.nick);

                // do the stuff with received object
                scores = new DB();

                scores.newScore(in.nick, in.score);


                //read score
                List<PlayerScore> scoresList = scores.selectScore();

                int best = 0, myBest = 0;
                String bestNick = null;

                for(PlayerScore c: scoresList)
                {
                    if(best < c.getScore())
                    {
                        best = c.getScore();
                        bestNick = c.getNick();
                    }

                    if(c.getNick().equals( in.nick) && myBest < c.getScore())
                    {
                        myBest = c.getScore();
                    }
                }

                // send back

                msg("sendin: " + myBest + "of " + in.nick + ", " + best + " of " + bestNick);
                ScoreIOObject out = new ScoreIOObject(0, in.nick, myBest, bestNick, best);
                msg("sent");

                oos.writeObject(out);
            }




            // close socket
            synchronized (serverMainThread.locker) {
                serverMainThread.clients --;
            }
        } catch (IOException e) {
            msg("socket error");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            msg("can't identify class");
        } catch (SQLException e) {
            msg("SQLException occured");
            e.printStackTrace();
        }
        finally
        {
            closeObject(scores);

            closeObject(is);
            closeObject(os);
            closeObject(oos);
            closeObject(clientSocket);
        }

        msg("exit");

    }

    private void closeObject(Closeable obj)
    {
        if(obj == null)
            return;

        try {
            obj.close();
        } catch (IOException e) {
            System.out.print("failed to close");
            e.printStackTrace();
        }
    }
}
