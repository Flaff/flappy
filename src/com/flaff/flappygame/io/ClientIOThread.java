package com.flaff.flappygame.io;

import java.io.*;
import java.lang.reflect.Type;
import java.net.Socket;

import com.flaff.flappygame.Main;
import com.flaff.flappygame.level.scene.ScoreScreen;
import com.sun.corba.se.spi.activation.InvalidORBid;
import org.lwjgl.Sys;

public class ClientIOThread implements Runnable {

	Socket s = null;
	OutputStream os = null;
	ObjectOutputStream oos = null;
	InputStream is = null;
	ObjectInputStream ois = null;
	ScoreIOObject in = null;

	TypeIO type = TypeIO.UNDEFINED;

	public void run ()
	{
		try {
			s = new Socket(Main.me.getServer(),ScoreScreen.port);

			switch (type){
				case UNDEFINED:
					System.out.println("no task specified - use constructor with TypeIO parameter");
					break;
				case SINGLE_SCORE:
					single_SCORE(oos);
					break;
				case VERSUS_MYNAMEIS:
					versus_MYNAMEIS(oos, ois);
					break;
			}


			System.out.println("done i guess");

		} catch (IOException e) {
			System.out.println("client ioexception - server offline?");
		} catch (ClassNotFoundException e) {
			System.out.println("client class not found exception - the class may not be present");
		} catch(Exception e){
            System.out.println("exception " +e);
        }
		finally
		{
			closeObject(oos);
			closeObject(os);
			closeObject(is);
			closeObject(s);
		}

	}

	private void closeObject(Closeable obj)
	{
		if(obj == null)
			return;

		try {
			obj.close();
		} catch (IOException e) {
			System.out.print("failed to close");
			e.printStackTrace();
		}
	}

	private void versus_MYNAMEIS(ObjectOutputStream oos, ObjectInputStream ois) throws Exception
	{
		os = s.getOutputStream();
		os.flush();
		oos = new ObjectOutputStream(os);

        System.out.println("sending "+TypeIO.VERSUS_MYNAMEIS + " " + Main.me.name);
		// tell server who you are
		oos.writeObject(new ScoreIOObject(TypeIO.VERSUS_MYNAMEIS, Main.me.name));
		System.out.println("sent");
		// response

		is = s.getInputStream();
		ois = new ObjectInputStream(is);
		os = s.getOutputStream();
		os.flush();
		//oos = new ObjectOutputStream(os);


		ScoreIOObject receive = (ScoreIOObject) ois.readObject();

		Main.opponent.name = receive.nick;

        System.out.println("received "+receive.type);
        // show results of game
        if(receive.type == TypeIO.VERSUS_WIN || receive.type == TypeIO.VERSUS_LOSE || receive.type == TypeIO.VERSUS_REFUSE) {
            Main.mode = receive.type;
        }

        if(receive.type == TypeIO.VERSUS_CHALLENGE)
        {
            Main.mode = receive.type;
            versus_RESPOND(oos, receive);
            return;
        }

        if(receive.type == TypeIO.VERSUS_CANPLAY)
        {
            Main.mode = receive.type;
            versus_CHALLENGE(oos);
        }
	}

    private void versus_RESPOND(ObjectOutputStream oos, ScoreIOObject receive) throws InterruptedException, IOException
    {
        Main.opponent.name = receive.nick;
        Main.opponent.score = receive.score;

		System.out.println("waiting to send ("+Main.mode+")");

        // wait until player finishes playing
        while(!(Main.mode.equals(TypeIO.VERSUS_CHALLENGE_SEND) || Main.mode.equals(TypeIO.VERSUS_REFUSE)))
            Thread.sleep(100);

		TypeIO sendType = TypeIO.UNDEFINED;
		if(Main.mode.equals((TypeIO.VERSUS_CHALLENGE_SEND)))
			sendType = TypeIO.VERSUS_CHALLENGE_RESPOND;
		else sendType = TypeIO.VERSUS_REFUSE;

		System.out.println("sending type:" + sendType + ", from:" + Main.me.name + ", to:" + receive.nick + ", score:" + Main.me.score);

        // send challenge back as response
//		is = s.getInputStream();
//		ois = new ObjectInputStream(is);
		os = s.getOutputStream();
		os.flush();
//		oos = new ObjectOutputStream(os);

        oos.writeObject(
                new ScoreIOObject(sendType, Main.me.name, receive.nick, Main.me.score)
        );
		System.out.println("sent");
    }

    private void versus_CHALLENGE(ObjectOutputStream oos) throws InterruptedException, IOException
    {
        // wait until player finishes playing
        while(Main.mode != TypeIO.VERSUS_CHALLENGE_SEND)
            Thread.sleep(100);

        os.flush();
        oos.writeObject(
                new ScoreIOObject(TypeIO.VERSUS_CHALLENGE_SEND, Main.me.name, Main.opponent.name, Main.me.score)
        );

    }

	private void single_SCORE(ObjectOutputStream oos) throws IOException, ClassNotFoundException
	{
		// upload score
		os = s.getOutputStream();
		os.flush();
		oos = new ObjectOutputStream(os);

		System.out.println("sendin: " + Main.me.name + " " + Main.me.score);
		oos.writeObject(new ScoreIOObject(TypeIO.SINGLE_SCORE, Main.me.name, Main.me.score));

		// get scores back
		is = s.getInputStream();
		ois = new ObjectInputStream(is);

		System.out.println("sent, waitin");
		in = (ScoreIOObject) ois.readObject();

		if(in==null) {
			System.out.println("received damaged or empty info");
			return;
		}

		System.out.println("i has: " + in.score + "of " + in.nick + ", " + in.score2 + " of " + in.nick2);
		ScoreScreen.myBest = in.score;
		ScoreScreen.best = in.score2;
		ScoreScreen.bestNick = in.nick2;
	}

	public ClientIOThread(TypeIO type)
	{
		this.type = type;
	}
}
