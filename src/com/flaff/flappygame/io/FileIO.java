package com.flaff.flappygame.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import org.newdawn.slick.util.ResourceLoader;

import com.flaff.flappygame.Player;
import com.flaff.flappygame.level.object.Bird;

public class FileIO {

	static public String resourceFolder = "resources/";
	

	public static InputStream getFont(String name)
	{
		InputStream iStream = null;

		iStream = ResourceLoader.getResourceAsStream(resourceFolder + name + ".ttf");
		return iStream;
	}

	
	public static InputStream getTexture(String name)
	{
		InputStream iStream = null;

		iStream = ResourceLoader.getResourceAsStream(resourceFolder + name + ".png");
		return iStream;
	}
	
	
	public void changeSourceFolder(String resourceFolder)
	{
		FileIO.resourceFolder = resourceFolder;
	}
	
	
	public static void save(Player player)
	{
		File file = new File("save.dat");

		PrintWriter writer;
		try {
			writer = new PrintWriter(file);
				writer.println(player.getName());
				writer.println(player.getBird().getColor());
				writer.println(player.getServer());
			writer.close();
		} catch (FileNotFoundException e1) {
			System.out.println("cant write to save.dat (read only?)");
			e1.printStackTrace();
		}
	}
	
	public static Player load()
	{
		File file = new File("save.dat");
		Scanner scanner;
		
		try {
			scanner = new Scanner(file);
			String name = scanner.nextLine();
			int color = scanner.nextInt()%10; // modulo zabezpiecza przed nielegalnymi wartosciami
			String server = scanner.nextLine();

			scanner.close();
			
			return new Player(name, new Bird(color), server );

		} catch (FileNotFoundException e) {
			// tworzy plik jak go nie ma
			PrintWriter writer;
			try {
				writer = new PrintWriter(file);
				writer.println("");
				writer.println("0");
				writer.println("127.0.0.1");
				writer.close();
				
				scanner = new Scanner(file);
				String name = scanner.nextLine();
				int color = Integer.parseInt(scanner.nextLine());
				String server = scanner.nextLine();
				scanner.close();

				return new Player(name, new Bird(color), server );

			} catch (FileNotFoundException e1) {
				System.out.println("cant write to save.dat (read only or insufficient privilleges)");

				return null;
			}
		}
	}
}
