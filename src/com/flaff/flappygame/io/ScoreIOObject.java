package com.flaff.flappygame.io;

import java.io.Serializable;

public class ScoreIOObject implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public int score = -1, score2 = -1;
	public String nick = null, nick2 = null;
	public TypeIO type = TypeIO.UNDEFINED;

	// 	   -2 - undefined
    //     -1 - reveal yourself (nick)
	//		0 - send score single (score, nick)
	//		1 - receive score single (score - player's best, nick2 - best player's nick, score2 - bestplayer's score)

	//		2 - receive challenge (nick - who challenged, score - challenger's score)
	//		3 - no challenge received response ()
	//		4 - send score response (score, nick, nick2 - challenger)
	//		5 - send score challenge (score, nick, nick2 - challenged)
	//		6 - no challenge results
	//		7 - receive challenge result win
	//		8 - receive challenge result lose

	//		versus
    //          reveal yourself
    //                      (-1)-> nick
    //
	//			received challenge results?
	//				yes		(7/8)<- show win/lose then show challenge screen
	//				no		(6)<-	proceed to check challenges

	//			received challenge?	//
	//				yes		before  (2)<-	show challenger's name and start
	//				 	    after   (4)->   show comparsion to received score(win/lose) and send back(4) player's score	//
	// 				no 	   	before  (3)<-   let player type who to challenge and start
	//						after   (5)->	send score back
	//
	//		single
	//			 	before      ( )--   do nothing
	//				after       (0)->	send your score
	//							(1)<-   receive back highscores





    // 1 receive score single
    public ScoreIOObject(TypeIO type, String nick, int score, String nick2, int score2)
    {
        this.type = type;
        this.nick = nick;
        this.score = score;
        this.nick2 = nick2;
        this.score2 = score2;
    }

    // 0 single score
    // 2 versus receive challenge
    public ScoreIOObject(TypeIO type, String nick, int score)
    {
        this.type = type;
        this.nick = nick;
        this.score = score;
    }

    // 3/6 versus no challenge received/no challenge results
    public ScoreIOObject(TypeIO type)
    {
        this.type = type;
    }

    // -1 versus reveal yourself
    // 7/8 versus receive win/lose
    public ScoreIOObject(TypeIO type, String nick)
    {
        this.type = type;
        this.nick = nick;
    }

    public ScoreIOObject(TypeIO type, String from, String to, int score)
    {
        this.type = type;
        this.nick = from;
        this.nick2 = to;
        this.score = score;
    }

    // 4/5 versus send score response/challenge
    public ScoreIOObject(TypeIO type, String nick, int score, int score2)
    {
        this.type = type;
        this.nick = nick;
    }
}
