package com.flaff.flappygame;

import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.List;

import com.flaff.flappygame.io.TypeIO;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import com.flaff.flappygame.level.graphics.Color3f;
import com.flaff.flappygame.level.graphics.DTexture;
import com.flaff.flappygame.level.graphics.Rectangle;
import com.flaff.flappygame.level.graphics.Screen;
import com.flaff.flappygame.level.graphics.TTFont;
import com.flaff.flappygame.io.FileIO;
import com.flaff.flappygame.io.Input;
import com.flaff.flappygame.level.scene.Level;
import com.flaff.flappygame.level.scene.Menu;
import com.flaff.flappygame.level.scene.ScoreScreen;

public class Main implements Runnable {


	// watek, w ktorym zostanie uruchomiona gra
	private Thread thread;

	// zmienna decydujaca o tym, ktora scena bedzie wyswietlana. np. ekran wynikow czy sam poziom
	public static int level = 0;

	public static int versusTimer = 0;

	// struktura player przechowujaca informacje o graczu
	public static Player me, opponent;
	public static TTFont font;
	public static TypeIO mode = TypeIO.UNDEFINED;
	public static boolean iRejected = false;

	// bool, ktory mial decydowac o wygladzie gry - istnieja dodatkowe tekstury do "nawiedzonego" trybu, ktory nie zostal zaimplementowany
	public static boolean haunted = false;

	// lista wszystkich odmian kolorystycznych gracza
	public static List<DTexture> birdTextures = new ArrayList<DTexture>(9);
	
	// dodaje do listy kolory
	private void init()
	{
		Color3f.init();			// wczytuje kolroy do listy potrzebne do wyswetialnia wyboru koloru
		Menu.init();			// prepare all objects needed in menu
		Level.init();
		ScoreScreen.init();
		me = FileIO.load();		// load player data from file
        opponent = new Player();
	}

	public void start()
	{
		thread = new Thread(this, "Game");
		thread.start();
	}

	public void run()
	{
		initGL();
		init();
		
		while(!Display.isCloseRequested()){
			
			if(level == 0)
				renderMenu();
			else if(level == 1)
				renderLevel();
			else if(level == -1)
				renderScoreScreen();
			else if(level == -2)
				System.exit(0);

			Display.update();
			Display.sync(60);

			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

			Display.destroy();
			System.exit(0);
	}
	
	
	public void initGL()
	{
		// tworzenie okna oraz inicjalizacja openGL
		try {
			Display.setDisplayMode(new DisplayMode(Screen.getWidth(), Screen.getHeight()));
			Display.setTitle("Flappy");
			Display.create();
		} catch (LWJGLException e) {
			System.out.println("error:" + e);
	
			e.printStackTrace();
			return;
		}
		
	    GL11.glEnable(GL_TEXTURE_2D);
       
        GL11.glShadeModel(GL_SMOOTH);       
        GL11.glMatrixMode(GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
        GL11.glOrtho(0, Screen.getWidth(), Screen.getHeight(), 0, 1, -1);
        GL11.glMatrixMode(GL_MODELVIEW);
		
        GL11.glEnable(GL11.GL_BLEND);
	}
	

	// lista funkcji wyswietlajacych sceny
	
	public void  renderMenu()
	{
		Input.update(); // funkcja do przechwytywania klawiszy, dodaje je do stringu
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		Rectangle.draw(0,0,Screen.getWidth(),Screen.getHeight(),new Color3f(138,210,255));
		Menu.draw();
	}


	public void renderLevel()
	{
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		
		Rectangle.draw(0,0,Screen.getWidth(),Screen.getHeight(),new Color3f(138,210,255));
		Level.draw();
	}
	
	public void renderScoreScreen()
	{
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		Rectangle.draw(0, 0, Screen.getWidth(), Screen.getHeight(), new Color3f(138, 210, 255));
		ScoreScreen.draw();
	}

	public static void main(String[] args) {
		new Main().run();
	}

}
