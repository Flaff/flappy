package com.flaff.flappygame.level.scene;

import java.util.ArrayList;
import java.util.List;

import com.flaff.flappygame.Main;
import com.flaff.flappygame.io.TypeIO;
import com.flaff.flappygame.level.graphics.Screen;
import com.flaff.flappygame.level.graphics.Text;
import com.flaff.flappygame.io.Input;
import com.flaff.flappygame.level.object.Cloud;
import com.flaff.flappygame.level.object.Pipe;

public class Level {
	//private static int currentState = 0;
	private int state = 0;
	private int difficulty = 0;
	
	private static int firstPipe = 0;
	private static int lastPipe, nextPipe;
	
	private static int firstCloud = 0;
	private static int lastCloud;
	
	public static int pipesAmount = 6;
	public static List<Pipe> pipesUp = new ArrayList<Pipe>(pipesAmount);
	public static List<Pipe> pipesDn = new ArrayList<Pipe>(pipesAmount);
	public static int pipeDistance = Screen.getWidth()/(pipesAmount*2) - 150;
	
	public static int cloudsAmount = 12;
	public static List<Cloud> cloudsDn = new ArrayList<Cloud>(cloudsAmount);
	public static int cloudDistance = 50;
	
	private static int velocity = 10;
	private static int scoreGuard = 99;

	public static int time = 60 * 60;
	
	public static void reinit()
	{
		// position pipes
		for(int i=0; i<pipesAmount; i++)
		{
			int random = (int)(Math.random()*400);
			pipesUp.get(i).setXY(Screen.getWidth() + (pipeDistance+Pipe.getWidth())*i, 670 - random);
			pipesDn.get(i).setXY(Screen.getWidth() + (pipeDistance+Pipe.getWidth())*i, 0 - random);
		}
		firstPipe = 0;
		lastPipe = pipesAmount-1;
		
		// position clouds
		for(int i=0; i<cloudsAmount; i++)
		{
			int random = (int)(Math.random()*150);
			cloudsDn.get(i).setXY(Screen.getWidth() + (cloudDistance-random+cloudsDn.get(i).getWidth())*i, 630 - random);
		}
		firstCloud = 0;
		lastCloud = cloudsAmount-1;

		time = 60 * 60; // 60seconds * 60fps
		Main.me.lives = 3;
		Main.me.score = 0;
	}
	
	public static void init()
	{
		Pipe.init();
		Cloud.init();

		
		// position pipes
		for(int i=0; i<pipesAmount; i++)
		{
			int random = (int)(Math.random()*400);
			
			pipesUp.add(new Pipe(1));
			pipesDn.add(new Pipe(0));
			pipesUp.get(i).setXY(Screen.getWidth() + (pipeDistance+Pipe.getWidth())*i, 670 - random);
			pipesDn.get(i).setXY(Screen.getWidth() + (pipeDistance+Pipe.getWidth())*i, 0 - random);
		}
		lastPipe = pipesAmount-1;
		
		// position clouds
		for(int i=0; i<cloudsAmount; i++)
		{
			int random = (int)(Math.random()*150);
			cloudsDn.add(new Cloud());
			cloudsDn.get(i).setXY(Screen.getWidth() + (cloudDistance-random+cloudsDn.get(i).getWidth())*i, 630 - random);
		}
		lastCloud = cloudsAmount-1;
		
	}
	
	public static void draw()
	{
		if( Main.me.getBird().y == 0)
			Main.me.getBird().y = Screen.getHeight()/2;

		
		Main.me.getBird().scale = 0.6F;
		
		if(Main.me.getBird().y < 0 || Main.me.getBird().y > Screen.getHeight()-30)
		{
			// die
			Main.me.getBird().y = (int)pipesUp.get(nextPipe).y-130;
			Main.me.getBird().delta = 0;
			Main.me.lives --;
		}
		
		pipesDn.get(firstPipe);
		if(Main.me.getBird().x > pipesUp.get(nextPipe).x + 130
				&& Main.me.getBird().x < pipesUp.get(nextPipe).x + 285)
		{
			if(scoreGuard != nextPipe)
			{
				scoreGuard = nextPipe;
				Main.me.score++;
			}
			
			if( Main.me.getBird().y >= pipesUp.get(nextPipe).y-20
				|| Main.me.getBird().y <= pipesUp.get(nextPipe).y-200)
			{// die
				Main.me.getBird().y = (int)pipesUp.get(nextPipe).y-130;
				Main.me.getBird().delta = 0;
				Main.me.lives --;
			}
		}
		

		
		nextPipe = (firstPipe)%pipesAmount;

		Main.me.getBird().delta = Main.me.getBird().delta + 0.32F;
		
		if(Input.checkKey(0x1C) || Input.checkKey(0x39))
			Main.me.getBird().delta = -3.3F;
		
		if(Input.checkKey(0x01))
		{
			Main.level = -2;
		}
 		
		
		Main.me.getBird().draw(0);
		
		Main.me.getBird().y += 5*Main.me.getBird().delta;
		

		// clouds drawing and moving left
		for(int i=0; i<cloudsAmount; i++)
		{
			cloudsDn.get(i).draw();
			cloudsDn.get(i).setX(cloudsDn.get(i).getX()-velocity*0.6F);
		}
		
		// pipes drawing and moving left
		for(int i=0; i<pipesAmount; i++)
		{
			pipesUp.get(i).draw();
			pipesDn.get(i).draw();
			pipesUp.get(i).setX(pipesDn.get(i).getX()-velocity);
			pipesDn.get(i).setX(pipesDn.get(i).getX()-velocity);
			
		}
		
		
		// pipes loop
		if(pipesDn.get(firstPipe).getX() < -Pipe.getWidth())
		{
			
			int random = (int)(Math.random()*400);
			pipesUp.get(firstPipe).setXY( pipesDn.get(lastPipe).getX()+pipeDistance+Pipe.getWidth(), 670 - random);
			pipesDn.get(firstPipe).setXY( pipesUp.get(lastPipe).getX()+pipeDistance+Pipe.getWidth(), 0 - random);
		
			if(firstPipe != pipesAmount-1){
				lastPipe = firstPipe;
				firstPipe++;
			} else {
				firstPipe = 0;
				lastPipe = pipesAmount-1;
			}
		}


		
		// clouds loop
		if(cloudsDn.get(firstCloud).getX() < -cloudsDn.get(firstCloud).getWidth())
		{
			
			int random = (int)(Math.random()*150);
			cloudsDn.get(firstCloud).randomize();
			cloudsDn.get(firstCloud).setXY( cloudsDn.get(lastCloud).getX()+cloudDistance-random+cloudsDn.get(lastCloud).getWidth(), 630 - random);
		
			if(firstCloud != cloudsAmount-1){
				lastCloud = firstCloud;
				firstCloud++;
			} else {
				firstCloud = 0;
				lastCloud = cloudsAmount-1;
			}
		}
		
		//lives hardcodede
		if(Main.me.lives == 3)
			Text.drawText("<3 <3 <3",10, 10, 1F,0.31F,0.53F);
		else if(Main.me.lives == 2)
			Text.drawText("<3 <3",10, 10, 1F,0.31F,0.53F);
		else if(Main.me.lives == 1)
			Text.drawText("<3",10, 10, 1F,0.31F,0.53F);
		else
		{
			if(Main.mode == TypeIO.VERSUS_CHALLENGE)
				Main.mode = TypeIO.VERSUS_CHALLENGE_SEND;
			else if(Main.mode == TypeIO.SINGLE_SCORE)
				Main.mode = TypeIO.SINGLE_HIGHSCORES;
			Main.level = -1;
			Menu.onceConn = false;
		}
		Text.drawText(Integer.toString(Main.me.score),80);

		if(Main.mode == TypeIO.VERSUS_CHALLENGE) {
			Text.drawText(Integer.toString((time / 60)), 80, 200);
			if(time <= 0) {
				Menu.onceConn = false;
				Main.me.lives = 0;
				Main.mode = TypeIO.VERSUS_CHALLENGE_SEND;
				Main.level = -1;
			}
			time -= 2;
		}


	}
	
	
	public int getState()
	{
		return this.state;
	}
	
	public int getDifficulty()
	{
		return this.difficulty;
	}
}
