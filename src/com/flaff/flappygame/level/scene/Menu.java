package com.flaff.flappygame.level.scene;

import com.flaff.flappygame.Main;
import com.flaff.flappygame.io.ClientIOThread;
import com.flaff.flappygame.io.FileIO;
import com.flaff.flappygame.io.Input;
import com.flaff.flappygame.io.TypeIO;
import com.flaff.flappygame.level.graphics.*;
import com.flaff.flappygame.level.object.CloudIntro;
import com.flaff.flappygame.level.object.PipeIntro;
import org.lwjgl.input.Mouse;

import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;

import static com.flaff.flappygame.io.FileIO.save;


public class Menu {
	private static int firstPipe = 0;
	private static int lastPipe;
	
	private static int firstCloud = 0;
	private static int lastCloud;
	
	public static int pipesAmount = 6;
	public static List<PipeIntro> pipesUp = new ArrayList<PipeIntro>(pipesAmount);
	public static List<PipeIntro> pipesDn = new ArrayList<PipeIntro>(pipesAmount);
	public static int pipeDistance = Screen.getWidth()/(pipesAmount*2) - 200;
	
	public static int cloudsAmount = 12;
	public static List<CloudIntro> cloudsDn = new ArrayList<CloudIntro>(cloudsAmount);
	public static int cloudDistance = 50;
	
	private static int velocity = 10;

	static List<Circle> circles = new ArrayList<Circle>(9);			
	
	
	public static void init()
	{
		// init textures
		PipeIntro.init();
		CloudIntro.init();
		
		// init circles
		int colorsNumber = 9;
		int colorsGap = 80;
		for(int i=0; i<Color3f.getCount(); i++)
			circles.add(new Circle(colorsGap*i + Screen.getWidth()/2 - colorsNumber*colorsGap /2 + 30,
					Screen.getHeight() - 100, Color3f.get(i)));

		// load textures
		for(int i=0; i<Color3f.getCount(); i++)
			Main.birdTextures.add(new DTexture("bird"+i+"_",3));
		
		// position pipes
		for(int i=0; i<pipesAmount; i++)
		{
			int random = (int)(Math.random()*400);
			
			pipesUp.add(new PipeIntro(1));
			pipesDn.add(new PipeIntro(0));
			pipesUp.get(i).setXY(Screen.getWidth() + (pipeDistance+PipeIntro.getWidth())*i, 620 - random);
			pipesDn.get(i).setXY(Screen.getWidth() + (pipeDistance+PipeIntro.getWidth())*i, 0 - random);
		}
		lastPipe = pipesAmount-1;
		
		// position clouds
		for(int i=0; i<cloudsAmount; i++)
		{
			int random = (int)(Math.random()*150);
			cloudsDn.add(new CloudIntro());
			cloudsDn.get(i).setXY(Screen.getWidth() + (cloudDistance-random+cloudsDn.get(i).getWidth())*i, 630 - random);
		}
		lastCloud = cloudsAmount-1;
	}
	
	
	public static void draw()
	{
		// pipes drawing and moving left
		for(int i=0; i<pipesAmount; i++)
		{
			pipesUp.get(i).draw();
			pipesDn.get(i).draw();
			pipesUp.get(i).setX(pipesDn.get(i).getX()-velocity);
			pipesDn.get(i).setX(pipesDn.get(i).getX()-velocity);
			
		}
		
		// clouds drawing and moving left
		for(int i=0; i<cloudsAmount; i++)
		{
			cloudsDn.get(i).draw();
			cloudsDn.get(i).setX(cloudsDn.get(i).getX()-velocity*0.6F);
		}
		
		// pipes loop
		if(pipesDn.get(firstPipe).getX() < -PipeIntro.getWidth())
		{
			
			int random = (int)(Math.random()*400);
			pipesUp.get(firstPipe).setXY( pipesDn.get(lastPipe).getX()+pipeDistance+PipeIntro.getWidth(), 620 - random);
			pipesDn.get(firstPipe).setXY( pipesUp.get(lastPipe).getX()+pipeDistance+PipeIntro.getWidth(), 0 - random);
		
			if(firstPipe != pipesAmount-1){
				lastPipe = firstPipe;
				firstPipe++;
			} else {
				firstPipe = 0;
				lastPipe = pipesAmount-1;
			}
		}
		
		// clouds loop
		if(cloudsDn.get(firstCloud).getX() < -cloudsDn.get(firstCloud).getWidth())
		{
			
			int random = (int)(Math.random()*150);
			cloudsDn.get(firstCloud).randomize();
			cloudsDn.get(firstCloud).setXY( cloudsDn.get(lastCloud).getX()+cloudDistance-random+cloudsDn.get(lastCloud).getWidth(), 630 - random);
		
			if(firstCloud != cloudsAmount-1){
				lastCloud = firstCloud;
				firstCloud++;
			} else {
				firstCloud = 0;
				lastCloud = cloudsAmount-1;
			}
		}

		switch(Main.mode)
		{
			case UNDEFINED:
				pickModeUI();
				break;
			case VERSUS_MYNAMEIS:
				versusCheck();
				break;
			case VERSUS_WIN:
				versusResults();
				break;
			case VERSUS_LOSE:
				versusResults();
				break;
			case VERSUS_REFUSE:
				if(!Main.iRejected)
					versusResults();
				else {
					versusCheck(); onceConn = false;
				}
				break;
			case VERSUS_CHALLENGE:
				versusChallengeRespond();
				break;
			case VERSUS_CANPLAY:
				versusChallenge();
				break;

		}

	}

    private static void pickModeUI()
    {
        for(Circle circle : circles)
        {

            circle.isMouseOver(Input.getMouseX(),Input.getMouseY());
            if(Mouse.isButtonDown(0) && circle.mouseOver)	{

                for(Circle circle2 : circles)	// set all circles to unselected
                    circle2.selected = false;

                Main.me.getBird().setColor(circle.getNumber());		// change bird color
                save(Main.me);			// save player
            }

            // check if is mouse over circle
            if(circle.getNumber() == Main.me.getBird().getColor())
            {
                circle.selected = true;
            }

            circle.draw();
        }

        Main.me.getBird().draw(Screen.getWidth()/2 - Main.me.getBird().getWidth()/2, 500);



        Main.me.poolName(); // odpytywanie klawiatury - dodaje do stringa
        if(Main.me.getName().length() == 0)
            Text.drawText("TYPE YOUR NICKNAME",300);
        else
            Text.drawText(Main.me.getName(),300);

        Text.drawText("press ENTER for versus or SPACE for singleplayer",400);

		if(Input.checkKey(0x39))  // start single
		{
			Main.level = 1;
			Main.mode = TypeIO.SINGLE_SCORE;
			FileIO.save(Main.me);
		}
		else if(Input.checkKey(0x1C))
		{
			Main.mode = TypeIO.VERSUS_MYNAMEIS;
		}
    }


	static String dialog = "Awaiting server response";
	static boolean onceConn = false;
	private static void versusCheck()
	{
		if(!onceConn) {
			new Thread(new ClientIOThread(TypeIO.VERSUS_MYNAMEIS)).start();
			onceConn = true;
		}

		Text.drawText(dialog, 400);
	}

	private static void versusResults()
	{
		if(Main.mode == TypeIO.VERSUS_WIN)
			Text.drawText("You have won against "+Main.opponent.name+ " that scored "+Main.opponent.score,300);
		else if(Main.mode == TypeIO.VERSUS_LOSE)
			Text.drawText("You have lost against "+Main.opponent.name+ " that scored "+Main.opponent.score,300);
		else if(Main.mode == TypeIO.VERSUS_REFUSE)
			Text.drawText(Main.opponent.name+" has rejected your challenge",300);

		Text.drawText("Press [A] to continue", 400);

		if(Input.checkKey(Input.KEY_A)) {
			Main.mode = TypeIO.VERSUS_MYNAMEIS;
			onceConn = false;
		}
	}

	private static void versusChallengeRespond() {
		Text.drawText(Main.opponent.name + " challenged you", 300);
		Text.drawText("Press [A] to accept", 400);
		Text.drawText("Press [X] to refuse", 470);

		if (Input.checkKey(Input.KEY_X)) {
			Level.reinit();
			Main.mode = TypeIO.VERSUS_REFUSE;
			Main.iRejected = true;
		}
		if(Input.checkKey(Input.KEY_A)) {
			Level.reinit();
            Main.mode = TypeIO.VERSUS_CHALLENGE;
            Main.level = 1;
        }
	}

    private static void versusChallenge()
    {
        Main.opponent.poolName(); // odpytywanie klawiatury - dodaje do stringa

        if(Main.opponent.getName().length() == 0)
            Text.drawText("TYPE OPPONENT'S NICKNAME",300);
        else
            Text.drawText(Main.opponent.getName(),300);

        Text.drawText("press SPACE once you're done",400);

        if ( Main.me.getName().equals(Main.opponent.getName()) )
            Text.drawText("damn, you can't challenge yourself fool!",500);
 		else if(Input.checkKey(0x39))  // start single
		{
			Main.level = 1;
			Main.mode = TypeIO.VERSUS_CHALLENGE;
		}
    }

}
