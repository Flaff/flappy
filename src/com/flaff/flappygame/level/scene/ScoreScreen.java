package com.flaff.flappygame.level.scene;

import java.util.ArrayList;
import java.util.List;

import com.flaff.flappygame.Main;
import com.flaff.flappygame.io.TypeIO;
import com.flaff.flappygame.level.graphics.Circle;
import com.flaff.flappygame.level.graphics.Color3f;
import com.flaff.flappygame.level.graphics.Screen;
import com.flaff.flappygame.level.graphics.Text;
import com.flaff.flappygame.io.Input;
import com.flaff.flappygame.io.ClientIOThread;
import com.flaff.flappygame.level.object.CloudIntro;
import com.flaff.flappygame.level.object.PipeIntro;

public class ScoreScreen {
	private static int firstPipe = 0;
	private static int lastPipe;
	
	private static int firstCloud = 0;
	private static int lastCloud;
	
	public static int pipesAmount = 6;
	public static List<PipeIntro> pipesUp = new ArrayList<PipeIntro>(pipesAmount);
	public static List<PipeIntro> pipesDn = new ArrayList<PipeIntro>(pipesAmount);
	public static int pipeDistance = Screen.getWidth()/(pipesAmount*2) - 200;
	
	public static int cloudsAmount = 12;
	public static List<CloudIntro> cloudsDn = new ArrayList<CloudIntro>(cloudsAmount);
	public static int cloudDistance = 50;
	
	private static int velocity = 10;

	static List<Circle> circles = new ArrayList<Circle>(9);			
	
	// db
	public static boolean onceDB = false;
	public static int myBest = 0;
	public static int best = 0;
	public static String bestNick = null;
	public static int port = 9001;

	static ClientIOThread ioThread;
	
	private static void onceDB()
	{
		onceDB = true;
		ioThread = new ClientIOThread(TypeIO.SINGLE_SCORE);
		new Thread(ioThread).start();
	}
	
	public static void init()
	{
		// init textures
		PipeIntro.init();
		CloudIntro.init();
		
		// init circles
		int colorsNumber = 9;
		int colorsGap = 80;
		for(int i=0; i<Color3f.getCount(); i++)
			circles.add(new Circle(colorsGap*i + Screen.getWidth()/2 - colorsNumber*colorsGap /2 + 30,
					Screen.getHeight() - 100, Color3f.get(i)));

		
		// position pipes
		for(int i=0; i<pipesAmount; i++)
		{
			int random = (int)(Math.random()*400);
			
			pipesUp.add(new PipeIntro(1));
			pipesDn.add(new PipeIntro(0));
			pipesUp.get(i).setXY(Screen.getWidth() + (pipeDistance+PipeIntro.getWidth())*i, 620 - random);
			pipesDn.get(i).setXY(Screen.getWidth() + (pipeDistance+PipeIntro.getWidth())*i, 0 - random);
		}
		lastPipe = pipesAmount-1;
		
		// position clouds
		for(int i=0; i<cloudsAmount; i++)
		{
			int random = (int)(Math.random()*150);
			cloudsDn.add(new CloudIntro());
			cloudsDn.get(i).setXY(Screen.getWidth() + (cloudDistance-random+cloudsDn.get(i).getWidth())*i, 630 - random);
		}
		lastCloud = cloudsAmount-1;
	}
	
	
	public static void draw()
	{
		// pipes drawing and moving left
		for(int i=0; i<pipesAmount; i++)
		{
			pipesUp.get(i).draw();
			pipesDn.get(i).draw();
			pipesUp.get(i).setX(pipesDn.get(i).getX()-velocity);
			pipesDn.get(i).setX(pipesDn.get(i).getX()-velocity);
			
		}
		
		// clouds drawing and moving left
		for(int i=0; i<cloudsAmount; i++)
		{
			cloudsDn.get(i).draw();
			cloudsDn.get(i).setX(cloudsDn.get(i).getX()-velocity*0.6F);
		}
		
		// pipes loop
		if(pipesDn.get(firstPipe).getX() < -PipeIntro.getWidth())
		{
			
			int random = (int)(Math.random()*400);
			pipesUp.get(firstPipe).setXY( pipesDn.get(lastPipe).getX()+pipeDistance+PipeIntro.getWidth(), 620 - random);
			pipesDn.get(firstPipe).setXY( pipesUp.get(lastPipe).getX()+pipeDistance+PipeIntro.getWidth(), 0 - random);
		
			if(firstPipe != pipesAmount-1){
				lastPipe = firstPipe;
				firstPipe++;
			} else {
				firstPipe = 0;
				lastPipe = pipesAmount-1;
			}
		}
		
		// clouds loop
		if(cloudsDn.get(firstCloud).getX() < -cloudsDn.get(firstCloud).getWidth())
		{
			
			int random = (int)(Math.random()*150);
			cloudsDn.get(firstCloud).randomize();
			cloudsDn.get(firstCloud).setXY( cloudsDn.get(lastCloud).getX()+cloudDistance-random+cloudsDn.get(lastCloud).getWidth(), 630 - random);
		
			if(firstCloud != cloudsAmount-1){
				lastCloud = firstCloud;
				firstCloud++;
			} else {
				firstCloud = 0;
				lastCloud = cloudsAmount-1;
			}
		}
		

		if(Main.mode == TypeIO.SINGLE_HIGHSCORES)
		{
			if (!onceDB)
				onceDB();

			Text.drawText("Your score is " + Main.me.score, 300);
			Text.drawText("Press SPACE to play again", 600);

			if (bestNick != null) {
				Text.drawText("Your best score is " + myBest, 400);
				Text.drawText("Best score of " + best + " belongs to " + bestNick, 500);
			} else {
				Text.drawText("Failed to connect to server", 400);
			}
			if (Input.checkKey(Input.KEY_SPACE)) {
				Level.reinit();
				onceDB = false;
				Main.level = 1;
			}
		}
		else if(Main.mode == TypeIO.VERSUS_CHALLENGE_SEND)
		{
			Text.drawText("Your score is " + Main.me.score, 300);
			Text.drawText(Main.opponent.name, 400);
			Text.drawText("Press SPACE to enter menu", 600);

			if (Input.checkKey(Input.KEY_SPACE)) {
				Main.mode = TypeIO.UNDEFINED;
				Main.level = 0;
			}
		}

		if (Input.checkKey(Input.KEY_ESCAPE)) {
			Main.level = -2;
		}
	}

}
