package com.flaff.flappygame.level.object;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

import com.flaff.flappygame.level.graphics.DTexture;
import com.flaff.flappygame.level.graphics.Screen;

public class PipeIntro {
	private float x,y;
	
	private int up;
	
	private static int width = 500;
	private static int height = 500;
	private static List<DTexture> texture = new ArrayList<DTexture>(2);
	
	public PipeIntro(int up)
	{
		this.up = up;
	}
	
	public void draw(int x, int y)
	{
		float scale = 1;
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
 		
 		Color.white.bind();
 		PipeIntro.texture.get(this.up).get().bind(); // GL11.glBind(texture.getTextureID());
         
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glTexCoord2f(0,0);
            GL11.glVertex2f(x,y);
            GL11.glTexCoord2f(1,0);
            GL11.glVertex2f(x+PipeIntro.width*scale, y); // bird.getTextureWidth() bird.getTexture	Height()
            GL11.glTexCoord2f(1,1);
            GL11.glVertex2f(x+PipeIntro.width*scale, y+PipeIntro.height*scale);
            GL11.glTexCoord2f(0,1);
            GL11.glVertex2f(x, PipeIntro.height*scale+y);
        GL11.glEnd();
 		
 		GL11.glDisable(GL11.GL_BLEND);
 		
 		if(this.x <  -PipeIntro.width){
 			this.x = Screen.getWidth()+PipeIntro.width;
 		}
	}
	
	public void draw()
	{
		float scale = 1;
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
 		
        Color.white.bind();
 		PipeIntro.texture.get(this.up).get().bind(); // GL11.glBind(texture.getTextureID());
         
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glTexCoord2f(0,0);
            GL11.glVertex2f(x,y);
            GL11.glTexCoord2f(1,0);
            GL11.glVertex2f(x+PipeIntro.width*scale, y); // bird.getTextureWidth() bird.getTexture	Height()
            GL11.glTexCoord2f(1,1);
            GL11.glVertex2f(x+PipeIntro.width*scale, y+PipeIntro.height*scale);
            GL11.glTexCoord2f(0,1);
            GL11.glVertex2f(x, PipeIntro.height*scale+y);
        GL11.glEnd();
 		
 		GL11.glDisable(GL11.GL_BLEND);
	}
	
	public void setXY(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void setX(float x)
	{
		this.x = x;
	}
	
	public float getX()
	{
		return this.x;
	}
	
	public float getY()
	{
		return this.y;
	}
	public static int getWidth()
	{
		return PipeIntro.width;
	}
	public static void init()
	{
		PipeIntro.texture.add(new DTexture("512/pipeintro0"));
		PipeIntro.texture.add(new DTexture("512/pipeintro1"));
	}	
}
