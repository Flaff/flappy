package com.flaff.flappygame.level.object;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

import com.flaff.flappygame.level.graphics.DTexture;
import com.flaff.flappygame.Main;

public class Bird {

	public int x = 50, y = 0;
	private int color = 0;
	DTexture texture;
	private int width = 100;
	private int height = 100;

	public float delta = 1F, scale = 1F;
	
	public Bird(int color)
	{
		this.color = color;
		this.texture = Main.birdTextures.get(color); 
	}
	
	public void setPosition(int x)
	{
		this.x = x;
	}
	
	public void setPosition(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void setColor(int color)
	{
		this.color = color;
		this.texture = Main.birdTextures.get(color);
	}
	
	public void draw(int x, int y)
	{
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
 		
        Color.white.bind();
 		this.texture.get().bind(); // GL11.glBind(texture.getTextureID());
 		
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glTexCoord2f(0,0);
            GL11.glVertex2f(x,y);
            GL11.glTexCoord2f(1,0);
            GL11.glVertex2f(x+this.width*scale, y); // bird.getTextureWidth() bird.getTexture	Height()
            GL11.glTexCoord2f(1,1);
            GL11.glVertex2f(x+this.width*scale, y+this.height*scale);
            GL11.glTexCoord2f(0,1);
            GL11.glVertex2f(x, this.height*scale+y);
        GL11.glEnd();
 		
 		GL11.glDisable(GL11.GL_BLEND);
	}
	
	public void draw(float angle)
	{
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
 		
        Color.white.bind();
 		this.texture.get().bind(); // GL11.glBind(texture.getTextureID());
         
        GL11.glBegin(GL11.GL_QUADS);
	        GL11.glTexCoord2f(0,0);
	        GL11.glVertex2f(x,y);
	        GL11.glTexCoord2f(1,0);
	        GL11.glVertex2f(x+this.width*scale, y); // bird.getTextureWidth() bird.getTexture	Height()
	        GL11.glTexCoord2f(1,1);
	        GL11.glVertex2f(x+this.width*scale, y+this.height*scale);
	        GL11.glTexCoord2f(0,1);
	        GL11.glVertex2f(x, this.height*scale+y);
        GL11.glEnd();
 		
 		GL11.glDisable(GL11.GL_BLEND);
	}
	

	public DTexture getDTexture()
	{
		return this.texture;
	}
	
	public int getColor()
	{
		return this.color;
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public int getWidth()
	{
		return this.width;
	}
	
	public int getHeight()
	{
		return this.height;
	}
}
