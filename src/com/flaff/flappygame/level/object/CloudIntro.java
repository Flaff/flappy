package com.flaff.flappygame.level.object;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

import com.flaff.flappygame.level.graphics.DTexture;
import com.flaff.flappygame.level.graphics.Screen;

public class CloudIntro {
	private float x,y;
	
	// amount of kinds of clouds
	private static int clouds = 3;
	
	private int width = 200;
	private int height = 200;
	private static List<DTexture> texture = new ArrayList<DTexture>(clouds);
	
	private int whichCloud = 0;
	
	public CloudIntro()
	{
		this.randomize();
	}
	
	public void draw(int x, int y)
	{
		float scale = 1;
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
 		
 		Color.white.bind();
 		CloudIntro.texture.get(this.whichCloud).get().bind(); // GL11.glBind(texture.getTextureID());
         
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glTexCoord2f(0,0);
            GL11.glVertex2f(x,y);
            GL11.glTexCoord2f(1,0);
            GL11.glVertex2f(x+this.width*scale, y); // bird.getTextureWidth() bird.getTexture	Height()
            GL11.glTexCoord2f(1,1);
            GL11.glVertex2f(x+this.width*scale, y+this.height*scale);
            GL11.glTexCoord2f(0,1);
            GL11.glVertex2f(x, this.height*scale+y);
        GL11.glEnd();
 		
 		GL11.glDisable(GL11.GL_BLEND);
 		
 		if(this.x <  -this.width){
 			this.x = Screen.getWidth()+this.width;
 		}
	}
	
	public void draw()
	{
		float scale = 1;
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
 		
        Color.white.bind();
 		CloudIntro.texture.get(this.whichCloud).get().bind(); // GL11.glBind(texture.getTextureID());
         
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glTexCoord2f(0,0);
            GL11.glVertex2f(x,y);
            GL11.glTexCoord2f(1,0);
            GL11.glVertex2f(x+this.width*scale, y); // bird.getTextureWidth() bird.getTexture	Height()
            GL11.glTexCoord2f(1,1);
            GL11.glVertex2f(x+this.width*scale, y+this.height*scale);
            GL11.glTexCoord2f(0,1);
            GL11.glVertex2f(x, this.height*scale+y);
        GL11.glEnd();
 		
 		GL11.glDisable(GL11.GL_BLEND);
	}
	
	public void randomize()
	{
		// initialised when cloud gets out of bounds and needs to be re-randomized
		this.whichCloud = (int)(Math.random()*(clouds-1));
		this.width = 200+(int)(Math.random()*150);
		this.height = this.width;
	}
	
	public void setXY(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void setX(float x)
	{
		this.x = x;
	}
	
	public float getX()
	{
		return this.x;
	}
	
	public float getY()
	{
		return this.y;
	}
	
	public int getWidth()
	{
		return this.width;
	}
	
	public static void init()
	{
		for(int i=0; i<clouds; i++)
			CloudIntro.texture.add(new DTexture("256/cloudintro"+i));
	}	
}
