package com.flaff.flappygame.level.object;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

import com.flaff.flappygame.level.graphics.Color3f;
import com.flaff.flappygame.level.graphics.DTexture;
import com.flaff.flappygame.level.graphics.Rectangle;
import com.flaff.flappygame.level.graphics.Screen;

public class Pipe {
	public float x,y;
	
	private int up;
	
	private static int width = 500;
	private static int height = 500;
	private static List<DTexture> texture = new ArrayList<DTexture>(2);
	
	public Pipe(int up)
	{
		this.up = up;
	}
	
	public void draw(int x, int y)
	{
		
		float scale = 1;

		
		Rectangle.draw(x,y,(int)(500),500,new Color3f(0,0,255));

		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
		
 		Color.white.bind();
 		Pipe.texture.get(this.up).get().bind(); // GL11.glBind(texture.getTextureID());
         
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glTexCoord2f(0,0);
            GL11.glVertex2f(x,y);
            GL11.glTexCoord2f(1,0);
            GL11.glVertex2f(x+Pipe.width*scale, y); // bird.getTextureWidth() bird.getTexture	Height()
            GL11.glTexCoord2f(1,1);
            GL11.glVertex2f(x+Pipe.width*scale, y+Pipe.height*scale);
            GL11.glTexCoord2f(0,1);
            GL11.glVertex2f(x, Pipe.height*scale+y);
        GL11.glEnd();
 		
 		GL11.glDisable(GL11.GL_BLEND);
 		
 		if(this.x <  -Pipe.width){
 			this.x = Screen.getWidth()+Pipe.width;
 		}
	}
	
	public void draw()
	{
		float scale = 1;
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
 		
        Color.white.bind();
 		Pipe.texture.get(this.up).get().bind(); // GL11.glBind(texture.getTextureID());
         
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glTexCoord2f(0,0);
            GL11.glVertex2f(x,y);
            GL11.glTexCoord2f(1,0);
            GL11.glVertex2f(x+Pipe.width*scale, y); // bird.getTextureWidth() bird.getTexture	Height()
            GL11.glTexCoord2f(1,1);
            GL11.glVertex2f(x+Pipe.width*scale, y+Pipe.height*scale);
            GL11.glTexCoord2f(0,1);
            GL11.glVertex2f(x, Pipe.height*scale+y);
        GL11.glEnd();
 		
 		GL11.glDisable(GL11.GL_BLEND);
	}
	
	public void setXY(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void setX(float x)
	{
		this.x = x;
	}
	
	public float getX()
	{
		return this.x;
	}
	
	public float getY()
	{
		return this.y;
	}
	public static int getWidth()
	{
		return Pipe.width;
	}
	public static void init()
	{
		Pipe.texture.add(new DTexture("512/pipe0"));
		Pipe.texture.add(new DTexture("512/pipe1"));
	}	
}
