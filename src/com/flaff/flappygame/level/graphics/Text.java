package com.flaff.flappygame.level.graphics;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

public class Text {
		
	private static TTFont font = new TTFont();

	
	@SuppressWarnings("deprecation")
	public static void drawText(String txt, int y, int x, float r, float g, float b)
	{
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
 		
 		float r2 = r, g2 = g, b2 = b;

 		if(r2 - 0.2F < 0)
 			r2 = 0;
 		else
 			r2 -= 0.2F;
 
 		if(g2 - 0.2F < 0)
 			g2 = 0;
 		else
 			g2 -= 0.2F;
		
 		if(b2 - 0.2F < 0)
 			b2 = 0;
 		else
 			b2 -= 0.2F;
 		
  		font.get().drawString(x+2, y+2, txt, new Color(r2,g2,b2));
 		font.get().drawString(x, y, txt, new Color(r,g,b));
 		
 		GL11.glDisable(GL11.GL_BLEND);
	}
	
	@SuppressWarnings("deprecation")
	public static void drawText(String txt, int y, int x)
	{
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
 		
  		font.get().drawString(x+2, y+2, txt, new Color(0.6F,0.6F,0.6F));
 		font.get().drawString(x, y, txt);
 		
 		GL11.glDisable(GL11.GL_BLEND);
	}
	
 	@SuppressWarnings("deprecation")
	public static void drawText(String txt, int y)
	{
 		
 		GL11.glBlendFunc(GL11.GL_SRC_ALPHA,GL11.GL_ONE_MINUS_SRC_ALPHA);
 		GL11.glEnable(GL11.GL_BLEND);
 		
 		font.get().drawString((Screen.getWidth()/2 - (font.get().getWidth(txt) / 2) +2), y+2, txt, new Color(0.6F,0.6F,0.6F));
 		font.get().drawString(Screen.getWidth()/2 - (font.get().getWidth(txt) / 2), y, txt);
 		
 		GL11.glDisable(GL11.GL_BLEND);
 	}

}
