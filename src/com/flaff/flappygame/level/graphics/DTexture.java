package com.flaff.flappygame.level.graphics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import com.flaff.flappygame.io.FileIO;

public class DTexture {

	private List<Texture> textures;
	private int dCounter = 0; // dynamic counter, ktora teksture teraz wysle
	private int count = 0;
	private boolean stateShiftable = false;
	
	private int width, height;
	
	public DTexture(String name)
	{
		try {
			this.textures = new ArrayList<Texture>(1);
			this.textures.add(TextureLoader.getTexture( "PNG", FileIO.getTexture(name) ));
			
			this.width = textures.get(0).getImageWidth();
			this.height = textures.get(0).getImageHeight();
		} catch (IOException e) {
			System.out.println("FileIO -> DTexture : can't load "+name);
			// TODO: przekazywanie exception do main
			e.printStackTrace();
		}

	}
	
	public DTexture(String name, int count)
	{
		this.count = count;
		this.textures = new ArrayList<Texture>(this.count);
		for(int i=0; i<count; i++)
		{
			try {
				//System.out.println("loading DTEXTURE "+name + "_" + i);

				this.textures.add(TextureLoader.getTexture("PNG", FileIO.getTexture(name + i )));
				this.width = textures.get(0).getImageWidth();
				this.height = textures.get(0).getImageHeight();

			} catch (IOException e) {
				System.out.println("cant get texture: "+name);
				e.printStackTrace();
			}
		}
	}
	
	public DTexture(String name, int cnt, boolean states)
	{
		this.count = cnt;
		this.stateShiftable = states;
		
		for(int i=0; i<count*(stateShiftable?1.0F:0F + 1); i++)
		{
			try {
				this.textures.add(TextureLoader.getTexture("PNG", FileIO.getTexture(name + i)));
			} catch (IOException e) {
				System.out.println("cant get texture: "+name);
				e.printStackTrace();
			}
		}
		
		this.width = textures.get(0).getImageWidth();
		this.height = textures.get(0).getImageHeight();

	}
	
	public int getWidth()
	{
		return this.width;
	}
	
	public int getHeight()
	{
		return this.height;
	}
		
	public Texture get()
	{
		if(this.dCounter < this.count-1)
			this.dCounter ++;
		else
			this.dCounter = 0;

		return this.textures.get(this.dCounter);
	}
}
