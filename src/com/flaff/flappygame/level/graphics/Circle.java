package com.flaff.flappygame.level.graphics;

import static org.lwjgl.opengl.GL11.*;


public class Circle
{
		private static int circleSize = 40;
		
		public int x, y;
		private Color3f color;
		
		public boolean selected = false;
		public boolean mouseOver = false;
		
		private static int count = 0;
		private int number;
		
		public Circle(int x, int y, Color3f color)
		{
			this.x = x;
			this.y = y;
			this.color = color;
			
			this.number = count;
			count ++;
		}
		
		public void DrawCircle(float cx, float cy, float r, int num_segments) 
		{ 
			float theta = 2 * (float) 3.1415926 / (float)num_segments ; 
			float c = org.naokishibata.sleef.FastMath.cosf(theta);
			float s = org.naokishibata.sleef.FastMath.sinf(theta);
			float t;

			float x = r;
			float y = 0; 
		  //  glEnable(GL_POLYGON_SMOOTH);
			glBegin(GL_POLYGON); 
			for(int ii = 0; ii < num_segments; ii++) 
			{ 
				glVertex2f(x + cx, y + cy);
				
				t = x;
				x = c * x - s * y;
				y = s * t + c * y;
			} 
			glEnd(); 
		}
	 	public void draw()
		{
			glColor4f(0.7F,0.7F,0.7F,1F);
			
			DrawCircle(this.x + circleSize/2 +1,
					this.y + circleSize/2 +2,
					circleSize/2
					+circleSize/4*(this.selected?1.0f:0.0f)
					+circleSize/6*(this.mouseOver?1.0f:0.0f) +2,
					15);

			glColor4f(1,1,1,1);
			
			DrawCircle(this.x + circleSize/2,
					this.y + circleSize/2,
					circleSize/2
					+circleSize/4*(this.selected?1.0f:0.0f)
					+circleSize/6*(this.mouseOver?1.0f:0.0f) +2,
					15);

			glColor4f(this.color.getRf(), this.color.getGf(), this.color.getBf(),0);
		
			DrawCircle(this.x + circleSize/2,
					this.y + circleSize/2,
					circleSize/2
					+circleSize/4*(this.selected?1.0f:0.0f)
					+circleSize/6*(this.mouseOver?1.0f:0.0f),
					15);
		}
	 	
	 	public int getNumber()
	 	{
	 		return this.number;
	 	}

		public void isMouseOver(int mouseX, int mouseY)
		{
			if ( mouseX > x && mouseX < x + circleSize && mouseY > y && mouseY < y + circleSize )
				this.mouseOver = true;
			else
				this.mouseOver = false;
		}
	
}
