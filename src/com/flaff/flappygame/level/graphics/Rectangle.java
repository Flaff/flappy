package com.flaff.flappygame.level.graphics;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.GL11;

public class Rectangle {
	public static void draw(int x, int y, int w, int h, Color3f color)
	{
		glColor4f(color.getRf(),color.getGf(),color.getBf(),1F);
		
	   GL11.glBegin(GL11.GL_QUADS);
           GL11.glVertex2f(x,y);
           GL11.glVertex2f(x+w,y);
           GL11.glVertex2f(x+w,y+h);
           GL11.glVertex2f(x,h+y);
       GL11.glEnd();
	}
}
