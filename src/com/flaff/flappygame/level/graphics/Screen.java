package com.flaff.flappygame.level.graphics;

public class Screen {

	private static int screenHeight = 720;
	private static int screenWidth = 1280;
	
	public static int getHeight()
	{
		return screenHeight;
	}
	
	public static int getWidth()
	{
		return screenWidth;
	}
	
	
	public void setScreen(int w, int h)
	{
		Screen.screenWidth = w;
		Screen.screenHeight = h;
	}
}
