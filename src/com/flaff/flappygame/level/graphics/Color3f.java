package com.flaff.flappygame.level.graphics;

import java.util.ArrayList;
import java.util.List;

public class Color3f {
	private float r,g,b;
	private static int count = 0;
	
	static List<Color3f> colors = new ArrayList<Color3f>(9);

	public Color3f(float r, float g, float b)
	{
		this.r = (float) r/255;
		this.g = (float) g/255;
		this.b = (float) b/255;
		count++;
	}
	
	public static void init()
	{
		// old color mechanism
		colors.add(new Color3f(51,204,255)); 	// cyan
		colors.add(new Color3f(51,102,255)); 	// blue
		colors.add(new Color3f(153,51,255)); 	// purple
		colors.add(new Color3f(255,51,153)); 	// rozowy
		colors.add(new Color3f(255,51,51));		// red
		colors.add(new Color3f(255,102,51));	// orange
		colors.add(new Color3f(255,153,51));	// yellow
		colors.add(new Color3f(153,204,0));		// green
		colors.add(new Color3f(0,204,153));		// marine
	}
	
	public void changeColor3f(float r, float g, float b)
	{
		this.r = r;
		this.g = g;
		this.b = b;
	}
	
	public static int getCount()
	{
		return Color3f.count;
	}
	
	public static Color3f get(int color)
	{
		return colors.get(color);
	}
	
	public float getRf()
	{
		return r;
	}
	public float getGf()
	{
		return g;
	}
	public float getBf()
	{
		return b;
	}
}
