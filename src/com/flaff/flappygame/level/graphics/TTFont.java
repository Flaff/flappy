package com.flaff.flappygame.level.graphics;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;

import org.newdawn.slick.TrueTypeFont;

import com.flaff.flappygame.io.FileIO;

@SuppressWarnings("deprecation")
public class TTFont {
	
	private static float defaultSize = 50f;
	private static String defaultName = "font";
	private Font font = null;
	private TrueTypeFont TTFont;
	
	
	public TTFont()
	{
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, FileIO.getFont(defaultName) );
			TTFont = new TrueTypeFont(font.deriveFont(defaultSize), true);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(font == null)
			{
				System.out.println("TTFONT: " + FileIO.resourceFolder + defaultName + ".ttf (404)");
				TTFont = new TrueTypeFont((new Font("Times New Roman",Font.BOLD,(int)defaultSize)).deriveFont(defaultSize),false);
			}
			
		}
	}

	public TTFont(String name)
	{
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, FileIO.getFont(name) );
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(font == null)
			{
				System.out.println("TTFONT: " + FileIO.resourceFolder + name + ".ttf (404)");			}
		}
	}
	
	
	public TrueTypeFont get()
	{
		return this.TTFont;
	}
	/*
	public TrueTypeFont get(float size)
	{
		return new TrueTypeFont(font.deriveFont(size), true);
	}
	
	public TrueTypeFont get(float size, boolean antialias)
	{
		return new TrueTypeFont(font.deriveFont(size), antialias);
	}
	*/
}
