package com.flaff.flappygame;

import com.flaff.flappygame.io.Input;
import com.flaff.flappygame.level.object.Bird;

public class Player {

	public String name = null;
	private Bird bird = null;
	public int score = 0;
	public int lives = 3;

	private String server = "127.0.0.1";


	public Player()
	{
		this.name = "";
	}

	public Player(String name, Bird bird)
	{
		this.name = name;
		this.bird = bird;
	}
	
	public Player(String name, Bird bird, String server)
	{
		this.name = name;
		this.bird = bird;
		this.server = server;
	}
	
	public void addScore()
	{
		this.score ++;
	}
	
	public void addScore(boolean negative)
	{
		if(!negative)
			this.score ++;
		else
			this.score --;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void poolName()
	{
		// check backspace
		if(Input.checkKey(0x0E))
		{
			if (this.name.length() != 0)
				this.name = this.name.substring(0,this.name.length()-1);
		}
		
		this.name += Input.fetchKey();
	}
	
	public int getScore()
	{
		return this.score;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Bird getBird()
	{
		return this.bird;
	}
	
	public String getServer()
	{
		return this.server;
	}
	
}
