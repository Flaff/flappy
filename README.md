# Flappy #

Prosta gra na bazie [FlappyBird](http://gamejolt.com/games/action/mlg-flappy-bird-420/37951/), która cieszy się dużą popularnością.
Flappy posiada nie tylko opcję wysyłania najlepszych wyników, ale także tryb pojedynków do zmierzenia się ze swoimi znajomymi.


### Okno - OpenGL ###

* Menu główne
* Rozgrywka
* Wynik Single Player
* Dialogi Versus

### I/O plików ###

* Zapisywanie ustawień gracza do pliku
* Wczytywanie zapisu przy uruchamianiu
* Tekstury
* Czcionki

### Współbieżność ###

* Wątek Input
* Wątek gry
* Wątek komunikacji z serwerem
* Rozdzielanie każdego gracza po stronie serwera na osobny wątek
* Rozbudowanie wątku komunikacji z serwerem o versus

### Baza danych - JDBC, SQLite ###

* Projekt zarządzania bazą danych
* Stworzenie bay danych 
* Przechowywanie najlepszych wyników powiązanych z nickami 
* Przechowywanie ilości rozgrywek przeprowadzonych przez gracza

### Komunikacja sieciowa ###

* Stworzenie modelu komunikacji serwer-klient
* Pobieranie najlepszych wyników
* Stworzenie pakietu wraz z jego typami identyfikującymi
* Wielowątkowość serwera - możliwość obsługi wielu klientów naraz
* Rozbudowanie modelu klient-serwer o możliwość versus

### Mechanizm gry ###

* Postać oraz jej tekstury w odmianach kolorów
* Teren, przeszkody oraz ich tekstury
* Tekstury postaci rozgrywki rozszerzonej
* Tekstury terenu rozgrywki rozszerzonej
* Dynamiczne generowanie przeszkód oraz chmur
* Kolizja, liczenie punktów